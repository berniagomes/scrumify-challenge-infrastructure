CREATE VIEW v_timesheetsapproval_montly AS
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS january,
    (0)::double precision AS february,
    (0)::double precision AS march,
    (0)::double precision AS april,
    (0)::double precision AS may,
    (0)::double precision AS june,
    (0)::double precision AS july,
    (0)::double precision AS august,
    (0)::double precision AS september,
    (0)::double precision AS october,
    (0)::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 1) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS february,
    (0)::double precision AS march,
    (0)::double precision AS april,
    (0)::double precision AS may,
    (0)::double precision AS june,
    (0)::double precision AS july,
    (0)::double precision AS august,
    (0)::double precision AS september,
    (0)::double precision AS october,
    (0)::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 2) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (0)::double precision AS february,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS march,
    (0)::double precision AS april,
    (0)::double precision AS may,
    (0)::double precision AS june,
    (0)::double precision AS july,
    (0)::double precision AS august,
    (0)::double precision AS september,
    (0)::double precision AS october,
    (0)::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 3) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (0)::double precision AS february,
    (0)::double precision AS march,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS april,
    (0)::double precision AS may,
    (0)::double precision AS june,
    (0)::double precision AS july,
    (0)::double precision AS august,
    (0)::double precision AS september,
    (0)::double precision AS october,
    (0)::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 4) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (0)::double precision AS february,
    (0)::double precision AS march,
    (0)::double precision AS april,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS may,
    (0)::double precision AS june,
    (0)::double precision AS july,
    (0)::double precision AS august,
    (0)::double precision AS september,
    (0)::double precision AS october,
    (0)::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 5) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (0)::double precision AS february,
    (0)::double precision AS march,
    (0)::double precision AS april,
    (0)::double precision AS may,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS june,
    (0)::double precision AS july,
    (0)::double precision AS august,
    (0)::double precision AS september,
    (0)::double precision AS october,
    (0)::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 6) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (0)::double precision AS february,
    (0)::double precision AS march,
    (0)::double precision AS april,
    (0)::double precision AS may,
    (0)::double precision AS june,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS july,
    (0)::double precision AS august,
    (0)::double precision AS september,
    (0)::double precision AS october,
    (0)::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 7) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (0)::double precision AS february,
    (0)::double precision AS march,
    (0)::double precision AS april,
    (0)::double precision AS may,
    (0)::double precision AS june,
    (0)::double precision AS july,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS august,
    (0)::double precision AS september,
    (0)::double precision AS october,
    (0)::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 8) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (0)::double precision AS february,
    (0)::double precision AS march,
    (0)::double precision AS april,
    (0)::double precision AS may,
    (0)::double precision AS june,
    (0)::double precision AS july,
    (0)::double precision AS august,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS september,
    (0)::double precision AS october,
    (0)::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 9) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (0)::double precision AS february,
    (0)::double precision AS march,
    (0)::double precision AS april,
    (0)::double precision AS may,
    (0)::double precision AS june,
    (0)::double precision AS july,
    (0)::double precision AS august,
    (0)::double precision AS september,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS october,
    (0)::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE (((tsb.month)::double precision = (10)::double precision) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (0)::double precision AS february,
    (0)::double precision AS march,
    (0)::double precision AS april,
    (0)::double precision AS may,
    (0)::double precision AS june,
    (0)::double precision AS july,
    (0)::double precision AS august,
    (0)::double precision AS september,
    (0)::double precision AS october,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS november,
    (0)::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 11) AND (tsb.contract IS NOT NULL))
UNION ALL
 SELECT tsb.year,
    tsb.resource,
    tsb.contract,
    tsb.code,
    tsb.description,
    (0)::double precision AS january,
    (0)::double precision AS february,
    (0)::double precision AS march,
    (0)::double precision AS april,
    (0)::double precision AS may,
    (0)::double precision AS june,
    (0)::double precision AS july,
    (0)::double precision AS august,
    (0)::double precision AS september,
    (0)::double precision AS october,
    (0)::double precision AS november,
    (((((tsb.analysis + tsb.design) + tsb.development) + tsb.test) + tsb.other))::double precision AS december
   FROM timesheetsapproval tsb
  WHERE ((tsb.month = 12) AND (tsb.contract IS NOT NULL));