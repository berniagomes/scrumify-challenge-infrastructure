$(document).ready(function() {
   var id = $("input[type=hidden][name='id']").val();
   
   initialize();
});
        
function initialize() {
	
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	formValidation();
}

function formSubmission() {   
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKFLOWS_SAVE}}]];
   
   ajaxpost(url, $("#workflows-form").serialize(), "", false, function() {
      initialize();
   });
}

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
    	  name : {
            identifier : 'name',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}

function createworkflowtransition(id) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKFLOWS_TRANSITION_NEW} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	
    $('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
    
    ajaxget(url, "div[id='modal']", function() {
        initialize();
    }, true);
}

function editWorkflowtransition(id) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKFLOWS_TRANSITION_UPDATE}}]] + '/' + id;
	
    ajaxget(url, "div[id='modal']", function() {
        initialize();
    }, true);
}


function deleteworkflowtransition(id) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKFLOWS_TRANSITION_DELETE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
    
    $("#modal-confirmation")
    	.modal({closable: false,
       		onApprove: function() {
       		   ajaxget(url, "div[id='workflowtransitions-list']", function() {
       	        initialize();
       	       }, false);
       		},
       		onDeny: function() {
       		}
    	})
    	.modal('show');         
 };