$(document).ready(function () {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_CREATE}}]];

  if (id != 0) {
    url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
  } else {
    CKEDITOR.replace('description', {
      customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]],
      toolbar: 'Basic',
      uiColor: '#CDD1D3'
    });
  }
  ajaxtab(true, url, false, function (settings) {
    switch (settings.urlData.tab) {
      case "info":
      default:
        getinfo(settings.urlData.tab);
        break;
    }
  });

  initialize();
});

function initialize() {

  $('.ui.radio.checkbox').checkbox();
  $('.ui.toggle.checkbox').checkbox();
  $('.ui.dropdown').dropdown();
  $('select.dropdown').dropdown();
  $('.ui.mini').popup({
    position: 'top center'
  });

  formValidation();
}

function getinfo(tab) {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_UPDATE_TAB} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + tab;

  ajaxget(url, "div[data-tab=" + tab + "]", function () {
    initialize();

    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
  });
};

function formValidation() {
  $('#userstories-form').form({
    on: 'blur',
    inline: true,
    fields: {
      name: {
        identifier: 'name',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      team: {
        identifier: 'team',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      }
    }
  });
  
  $("#userstoryedit-form").form({
    on: 'blur',
    inline: true,
    fields: {
      name: {
        identifier: 'name',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      description: {
        identifier: 'description',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      }
    },
    onSuccess: function (event) {
      event.preventDefault();
      
      var id = $("input[type=hidden][name='id']").val();
      var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_INFO_FIELD_UPDATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + campo;

      ajaxpost(url, $("#userstoryedit-form").serialize(), "", false, function () {
        initialize();
      });
    }
  });
}

function editCampo(campo, header) {
  var userstory = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_INFO_FIELD}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + userstory + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + campo + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + header;

  ajaxget(url, "div[id='modal']", function () {
    formSubmissionedit(campo);
    if (campo == 'description') {
      CKEDITOR.replace('description', {
        customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]],
        toolbar: 'Basic',
        uiColor: '#CDD1D3'
      });
    }
  }, true);
};

function formSubmissionedit(campo) {
  $("#userstoryedit-form").form({
    on: 'blur',
    inline: true,
    fields: {
      name: {
        identifier: 'name',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      },
      description: {
        identifier: 'description',
        rules: [{
          type: 'empty',
          prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      }
    },
    onSuccess: function (event) {
      event.preventDefault();
      
      for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
      }
      
      var id = $("input[type=hidden][name='id']").val();
      var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
      var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_INFO_FIELD_UPDATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + campo;
      
      ajaxpost(url, $("#userstoryedit-form").serialize(), "label[id='" + campo + "US']", false, function () {
        initialize();
      }, returnurl);
    }
  });
}

function createworkitem(id) {
  var userstory = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_WORKITEM}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + userstory + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id;

  ajaxget(url, "div[id='modal']", function () {
    $('.ui.dropdown').dropdown();
    $('select.dropdown').dropdown();

    CKEDITOR.replace('description', {
      customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]],
      toolbar: 'Basic',
      uiColor: '#CDD1D3'
    });

    $("#workitems-form").form({
      on: 'blur',
      inline: true,
      fields: {
        name: {
          identifier: 'name',
          rules: [{
            type: 'empty',
            prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
          }]
        },
        estimate: {
          identifier: 'estimate',
          rules: [{
            type: 'empty',
            prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
          }]
        },
        team : {
            identifier : 'team',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         }
      },
      onSuccess: function (event) {
        for (instance in CKEDITOR.instances) {
          CKEDITOR.instances[instance].updateElement();
        }
        event.preventDefault();
        
        var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_WORKITEM_SAVE}}]];
        ajaxpost(url, $("#workitems-form").serialize(), "div[id='workitems-list']", true, function () {
          initialize();
        });
      }
    });
  }, true);
};

function createNote(id) {
  var userStoryId = document.getElementById("userstoryId").value;
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_NOTES_NEW}}]] + '/' + userStoryId + "/" + id;

  ajaxget(url, "div[id='modal']", function () {
    CKEDITOR.replace('content', {
      customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]]
    });
    
    $('.ui.dropdown').dropdown();
    $('select.dropdown').dropdown();
    $("#userstoryNote-form").form({
      on: 'blur',
      inline: true,
      fields: {
        content: {
          identifier: 'content',
          rules: [{
            type: 'ckeditorvalidator',
            prompt: [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]],
            value: "textarea[id='content']"
          }]
        }
      },
      onSuccess: function (event) {
        event.preventDefault();
        var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_NOTES_SAVE}}]];
        ajaxpost(url, $("#userstoryNote-form").serialize(), "div[id='notes-list']", true, function () {
          initialize();
        });
      }
    });
  }, true);
};

function updateState(id, state) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_INFO_STATUS}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + state;

  ajaxget(url, "div[data-tab='info']", function () {
    $('.ui.dropdown').dropdown();
  }, false);
};

function updatePoints(id, point) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_INFO_STORYPOINTS}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + point;

  ajaxget(url, "div[data-tab='info']", function () {
    $('.ui.dropdown').dropdown();
  }, false);
};

function updateEpic(id, epic) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_INFO_EPIC}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + epic;

  ajaxget(url, "div[data-tab='info']", function () {
    $('.ui.dropdown').dropdown();
    $('.ui.mini').popup({
      position: 'top center'
    });
  }, false);
};

function updateTeam(id, team) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_INFO_TEAM}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + team;

  ajaxget(url, "div[data-tab='info']", function () {
    $('.ui.dropdown').dropdown();
  }, false);
};

function addtag(id) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_TAG_ADD}}]] + '/' + id;
  ajaxget(url, "div[id='modal']", function () {
    $('.ui.checkbox').checkbox();
    $('.ui.toggle.checkbox').checkbox();
    $("#tags-form").form({
      onSuccess: function (event) {
        event.preventDefault();
        var urlpost = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_TAG_SAVE}}]];
        ajaxpost(urlpost, $("#tags-form").serialize(), "div[id='tags-list']", true, function () {
          initialize();
        });
      }
    })
  }, true);
}

function filter() {
  var url  = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USERSTORIES_FILTER}}]]
      ajaxget(url, "div[id='modal']", function() {
    	  $('.ui.checkbox').checkbox();
    	  $('.ui.toggle.checkbox').checkbox();
    	  $('#filter-form').form({
    		  onSuccess : function(event) {
    			  event.preventDefault();               
    			  ajaxpost(url, $("#filter-form").serialize(), "div[id='list-userstories']", true, function() {
    				  initialize();
    			  });
    		  }
    	  });
      }, true);
  }