package pt.com.scrumify.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.TeamContract;
import pt.com.scrumify.database.entities.TeamContractPK;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.database.services.TeamContractService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.entities.TeamView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.TeamsHelper;

@Controller
public class TeamContractsController {
   
   @Autowired
   private ContractService contractsService;
   @Autowired
   private TeamContractService teamContractsService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TeamsHelper teamsHelper;
   
   /*
    * TAB CONTRACTS TEAM
   */
  @ApiOperation("Mapping to respond ajax requests for team contracts.")
  @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_READ + "') and @SecurityService.isMember('Team', #team)")
  @GetMapping( value = ConstantsHelper.MAPPING_TEAMS_CONTRACTS_AJAX + ConstantsHelper.MAPPING_PARAMETER_TEAM)
   public String contractTabTeam(Model model, @PathVariable int team) {
     TeamView teamView = new TeamView(this.teamsService.getOne(team));
     
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, teamView);
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, teamsHelper.listOfContracts(team));
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMCONTRACT, teamContractsService.getByTeam(teamsService.getOne(team)));

     return ConstantsHelper.VIEW_TEAMS_CONTRACTS;
  }
   
   /*
    * ADD TEAM CONTRACT 
    */
  @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_UPDATE + "') and @SecurityService.isMember('Team', #team)")
   @GetMapping(value = ConstantsHelper.MAPPING_TEAMCONTRACTS_SAVE + ConstantsHelper.MAPPING_PARAMETER_TEAM + ConstantsHelper.MAPPING_PARAMETER_CONTRACT)
   public String add(Model model, @PathVariable int team, @PathVariable int contract) {
      TeamContractPK teamContractPK = new TeamContractPK();
      teamContractPK.setContract(this.contractsService.getOne(contract));
      teamContractPK.setTeam(this.teamsService.getOne(team));
      
      TeamContract teamContract = new TeamContract();
      teamContract.setPk(teamContractPK);
      this.teamContractsService.save(teamContract);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMCONTRACT, this.teamContractsService.getByTeam(this.teamsService.getOne(team)));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, this.teamsHelper.listOfContracts(team));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, this.teamsService.getOne(team));
                  
      return ConstantsHelper.TEAMCONTRACTS_VIEW_INDEX;
   }

   /*
    * REMOVE TEAM CONTRACT 
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_UPDATE + "') and @SecurityService.isMember('Team', #team)")
   @GetMapping(value = ConstantsHelper.MAPPING_TEAMCONTRACTS_DELETE + ConstantsHelper.MAPPING_PARAMETER_TEAM + ConstantsHelper.MAPPING_PARAMETER_CONTRACT)
   public String delete(Model model, @PathVariable int team, @PathVariable int contract) {
      TeamContractPK teamContractPK = new TeamContractPK();
      teamContractPK.setContract(this.contractsService.getOne(contract));
      teamContractPK.setTeam(this.teamsService.getOne(team));
      
      TeamContract teamContract = new TeamContract();
      teamContract.setPk(teamContractPK);
      teamContractsService.delete(teamContract);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMCONTRACT, this.teamContractsService.getByTeam(this.teamsService.getOne(team)));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, this.teamsHelper.listOfContracts(team));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, this.teamsService.getOne(team));

      return ConstantsHelper.TEAMCONTRACTS_VIEW_INDEX;
   }      

}