package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TIMESHEETS_EXPORT)
@AssociationOverrides({ 
   @AssociationOverride(name = "pk.resource", joinColumns = @JoinColumn(name = "resource")),
   @AssociationOverride(name = "pk.month", joinColumns = @JoinColumn(name = "month")),
   @AssociationOverride(name = "pk.fortnight", joinColumns = @JoinColumn(name = "fortnight")),
   @AssociationOverride(name = "pk.project", joinColumns = @JoinColumn(name = "project")),
   @AssociationOverride(name = "pk.task", joinColumns = @JoinColumn(name = "task")),
   @AssociationOverride(name = "pk.tow", joinColumns = @JoinColumn(name = "tow"))
}) 
public class TimesheetExport implements Serializable {
   private static final long serialVersionUID = 3207059965420345225L;

   @Getter
   @Setter
   @EmbeddedId
   private TimesheetExportPK pk = new TimesheetExportPK();
   
   @Getter
   @Setter
   @Column(name = "hours", nullable = false)
   private long hours;
   
   public TimesheetExport(String resource, Integer month, Integer fortnight, String project, String task, String tow, long hours) {
      super();
      
      this.pk.setResource(resource);
      this.pk.setMonth(month);
      this.pk.setFortnight(fortnight);
      this.pk.setProject(project);
      this.pk.setTask(task);
      this.pk.setTow(tow);
      this.setHours(hours);
   }
}