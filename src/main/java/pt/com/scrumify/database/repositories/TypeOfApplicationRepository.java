package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.TypeOfApplication;

public interface TypeOfApplicationRepository extends JpaRepository<TypeOfApplication, Integer> {
}