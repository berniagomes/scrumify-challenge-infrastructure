package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.ContractProperty;
import pt.com.scrumify.database.entities.ContractPropertyPK;

public class ContractPropertyView {
   
   @Getter
   @Setter
   private ContractPropertyPK pk = new ContractPropertyPK();
   
   @Getter
   @Setter
   private String value;

   public ContractPropertyView() {
      super();
   }
   
   public ContractPropertyView(ContractProperty contractProperty) {
      super();
      
      this.setPk(contractProperty.getPk());
      this.setValue(contractProperty.getValue());

   }

   public ContractProperty translate() {
      ContractProperty contractProperty = new ContractProperty();
      
      contractProperty.setPk(this.getPk());
      contractProperty.setValue(this.getValue());
      
      return contractProperty;
   }
}