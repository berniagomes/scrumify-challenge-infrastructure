package pt.com.scrumify.entities;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.helpers.ConstantsHelper;

public class SubAreaView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(max=5, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String abbreviation;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(max=80, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String name;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Area area = new Area();

   public SubAreaView() {
      super();
   }
   
   public SubAreaView(SubArea subArea) {
      super();
      
      if (subArea!= null) {
         this.setId(subArea.getId());
         this.setAbbreviation(subArea.getAbbreviation());
         this.setName(subArea.getName());
         this.setArea(subArea.getArea());
      }
   }

   public SubArea translate() {
      SubArea subArea = new SubArea();
      
      subArea.setId(this.getId());
      subArea.setAbbreviation(this.getAbbreviation());
      subArea.setName(this.getName());
      subArea.setArea(this.getArea());
      
      return subArea;
   }
}