package pt.com.scrumify.helpers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.Vacation;
import pt.com.scrumify.database.services.TimesheetDayService;

@Service
public class TimesheetDayHelper {
   @Autowired
   private TimesheetDayService timesheetDaysService;
   
   public void addAbsence(Absence absence) {
      TimesheetDay day = timesheetDaysService.getOne(absence.getResource(), absence.getDay().getDate());
      day.incrementAbsence(absence.getHours());
      timesheetDaysService.save(day);
   }
   
   public void removeAbsence(Absence absence) {
      TimesheetDay day = timesheetDaysService.getOne(absence.getResource(), absence.getDay().getDate());
      day.decrementAbsence(absence.getHours());
      timesheetDaysService.save(day);
   }
   
   public void addTimeSpent(Resource resource, Date date, Integer hours) {
      TimesheetDay day = timesheetDaysService.getOne(resource, date);
      day.incrementTimeSpent(hours);
      timesheetDaysService.save(day);
   }
   
   public void removeTimeSpent(Resource resource, Date date, Integer hours) {
      TimesheetDay day = timesheetDaysService.getOne(resource, date);
      day.decrementTimeSpent(hours);
      timesheetDaysService.save(day);
   }
   
   public TimesheetDay addVacation(Vacation vacation) {
      TimesheetDay day = timesheetDaysService.getOne(vacation.getResource(), vacation.getDay().getDate());
      day.incrementVacation(day.getHours());
      
      return timesheetDaysService.save(day);
   }
   
   public TimesheetDay removeVacation(Vacation vacation) {
      TimesheetDay day = timesheetDaysService.getOne(vacation.getResource(), vacation.getDay().getDate());
      day.decrementVacation(day.getHours());
      
      return timesheetDaysService.save(day);
   }
}